import * as $ from 'jquery';
import WOW from 'wow.js';
import 'slick-carousel';
import 'slick-carousel/slick/slick.css';

// console.log('ENV', process.env.NODE_ENV);

$(() => {
  const wow = new WOW({
    animateClass: 'animated',
    offset: 0,
    mobile: true,
    live: true,
    callback(box) {
      // console.log(`WOW: animating <${box.tagName.toLowerCase()}>`);
    },
  });
  wow.init();

  $('.hotel__content').slick({
    dots: false,
    infinite: false,
    speed: 300,
    variableWidth: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });

  $('.cookies__btn').click(() => {
    $('.cookies').hide();
    window.localStorage.setItem('accept-cookie', 'true');
  });

  if (window.localStorage.getItem('accept-cookie')) {
    $('.cookies').hide();
  }

  let el = $('.switch');
  let cur = el.find('.current');
  let options = el.find('.options li');
  let content = $('#content');

// Open language dropdown panel

  el.on('click', function(e) {
    el.addClass('show-options');
  });


// Close language dropdown panel

  options.on('click', function(e) {
    e.stopPropagation();

    let newLang = $(this).data('lang');

    cur.find('span').text(newLang);
    content.attr('class', newLang);

    setLang(newLang);

    options.removeClass('selected');
    $(this).addClass('selected');

    setTimeout(function() {
      el.removeClass('show-options');
    }, 600);
  });


// Save selected options into Local Storage

  function getLang() {
    let lang;
    if (localStorage.getItem('currentLang') === null) {
      lang = cur.find('span').text();
    } else {
      lang = JSON.parse(localStorage.getItem('currentLang')).toLowerCase();
    }

    // console.log(lang);

    cur.find('span').text(lang);
    options.parent().find(`li[data-lang="${lang}"]`).addClass('selected');

    content.attr('class', lang);
  }

  getLang();

  function setLang(newLang) {
    localStorage.setItem('currentLang', JSON.stringify(newLang).toLowerCase());

    content.attr('class', newLang);

    // console.log('New language is: ' + newLang);
  }

  $(document).mouseup(function (e){
    var div = $(".switch");
    if (!div.is(e.target)
      && div.has(e.target).length === 0) {
      div.removeClass('show-options');
    }
  });
});
