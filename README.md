Make sure you have Node.js.
Install Dependencies npm install
Run project npm start
The project will be launched. The browser will be opened. Then enjoy the development.
If you need to upload files to the server, just run the command npm run build or yarn build and all the files will be in the folder ./dist

npm install	- install
npm run start - development
npm run build -	production
npm run fix	- fix file with linters
